# Payara embedded server maven config

This repo contains a set maven project that is configured to create a valid ear file in ``template-ear/target/target.ear`` for the deployment in a Payara/Glassfish server.

The code is compiled when executing:

> mvn clean install

It is setup for web development, ejb development, and persistence using some default values.

# Payara Embedded

Instead of installing a Glassfish/Payara application server, one could use an embedded version that can be started as a jar file (*fat jar*). Such an embedded server has been configured in the ``template-payara`` directory. When on Linux, the ``compileAndRun.sh`` script can be executed to first compile the source with maven and then start the server with the formerly ear mentioned above in one call. The script is simple and can easily be translated into batch, or whatnot.

Once the server has been started, the program can be tested by opening [this link](http://localhost:8085/template/TestServlet) following link in a browser of choice:

# User Rights

Feel free to customize the project at will.

Felix Dobslaw
