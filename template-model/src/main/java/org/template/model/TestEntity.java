package org.template.model;

import java.util.UUID;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class TestEntity {

	private String name;

	@Id
	private UUID id;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}

	@Override
	public String toString() {
		return new StringBuilder().append("TestEntity (name: ").append(name).append(')').toString();
	}
}
