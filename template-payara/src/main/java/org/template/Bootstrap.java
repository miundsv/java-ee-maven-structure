package org.template;

import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.glassfish.embeddable.GlassFish;
import org.glassfish.embeddable.GlassFishException;
import org.glassfish.embeddable.GlassFishProperties;
import org.glassfish.embeddable.GlassFishRuntime;

public class Bootstrap

{
	private static final int HTTP_PORT = 8085;

	private static final String CONNECTION_POOL_ID = "testdb";

	private static final String JDBC_RESOURCE_ID = "jdbc/" + CONNECTION_POOL_ID;

	private static final String USER_NAME = "postgres";

	private static final String PASSWORD = "password";

	private static final String POOL_PROPERTIES = "User=" + USER_NAME + ":Password=" + PASSWORD
			+ ":URL=jdbc\\:postgresql\\://localhost/" + CONNECTION_POOL_ID;

	public static void main(String[] args) throws IOException {
		try {
			GlassFishRuntime runtime = GlassFishRuntime.bootstrap();
			GlassFish glassfish = initializeAndStartGlassfishServer(runtime);
			createDatabasePool(glassfish);
			createJDBCResource(glassfish);
			deployApp(glassfish, args[0]);
		}

		catch (GlassFishException ex) {
			Logger.getLogger(Bootstrap.class.getName()).log(Level.SEVERE, null, ex);
		}
	}

	private static GlassFish initializeAndStartGlassfishServer(GlassFishRuntime runtime) throws GlassFishException {
		GlassFishProperties glassfishProperties = new GlassFishProperties();
		glassfishProperties.setPort("http-listener", HTTP_PORT);
		GlassFish glassfish = runtime.newGlassFish(glassfishProperties);
		glassfish.start();
		return glassfish;
	}

	private static void createDatabasePool(GlassFish glassfish) throws GlassFishException {
		glassfish.getCommandRunner().run("create-jdbc-connection-pool", "--restype",
				"javax.sql.ConnectionPoolDataSource", "--datasourceclassname", "org.postgresql.ds.PGSimpleDataSource",
				"--property", POOL_PROPERTIES, CONNECTION_POOL_ID);
	}

	private static void createJDBCResource(GlassFish glassfish) throws GlassFishException {
		glassfish.getCommandRunner().run("create-jdbc-resource", "--connectionpoolid", CONNECTION_POOL_ID,
				JDBC_RESOURCE_ID);
	}

	private static void deployApp(GlassFish glassfish, String appPath) throws IOException, GlassFishException {
		glassfish.getDeployer().deploy(new File(appPath));
	}
}