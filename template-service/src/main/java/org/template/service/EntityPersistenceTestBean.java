package org.template.service;

import java.util.UUID;
import java.util.logging.Logger;

import javax.ejb.LocalBean;
import javax.ejb.Startup;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.template.model.TestEntity;

@Stateless
@Startup
@LocalBean
public class EntityPersistenceTestBean {

	@PersistenceContext(unitName = "myservice")
	private EntityManager em;
	
	public EntityPersistenceTestBean() {
		Logger.getLogger(getClass().getName()).info("Test Bean initialized.");
	}

	public TestEntity persistTestEntity() {
		TestEntity entity = new TestEntity();
		entity.setName("Example Name");
		entity.setId(UUID.randomUUID());
		em.persist(entity);
		return entity;
	}

}