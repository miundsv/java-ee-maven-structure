package org.template.web;

import java.io.IOException;
import java.io.PrintWriter;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.template.service.EntityPersistenceTestBean;

@WebServlet("/TestServlet")
public class TestServlet extends HttpServlet {
	
	private static final long serialVersionUID = 1L;

	@EJB
	private EntityPersistenceTestBean service;

    public TestServlet() {
        super();
    }

    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	response.setContentType("text/html;charset=UTF-8");
    	try(PrintWriter out = response.getWriter()) {
    		out.println("<html><body>");
    		out.println("<h4>Welcome to this service</h4>");
    		out.println("<p>Before service</p>");
    		out.println("<p>Output from service: " + service.persistTestEntity() + "</p>");
    		out.println("<p>After service</p>");
    		out.println("</html></body>");
    	}catch(Exception e) {
    		throw e;
    	}
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	processRequest(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}

}
